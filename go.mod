module gitlab.com/pragmaticreviews/golang-amazon-polly

go 1.13

require (
	github.com/aws/aws-sdk-go v1.28.9
	google.golang.org/api v0.15.0
)
