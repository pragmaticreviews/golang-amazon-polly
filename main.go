package main

import "gitlab.com/pragmaticreviews/golang-amazon-polly/service"

var(
	kimberly service.PollyService = service.NewKimberlyPollyService()
	joey service.PollyService = service.NewJoeyPollyService()
)

func main() {
	err := kimberly.Synthesize("Hi, I am Kimberly.", "kimberly.mp3")

	if err != nil {
		panic(err)
	}

	err = joey.Synthesize("Hey guys, this is Joey.", "joey.mp3")

	if err != nil {
		panic(err)
	}
}
